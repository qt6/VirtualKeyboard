#include "virtualkeyboard.h"

VirtualKeyboard::VirtualKeyboard(const QWidget *p_parent,const int font_size)
{

    parent_widget=const_cast<QWidget *>(p_parent);


    initElements();
    fillMainLay(mainLayout);
    fillStage1(stage1);
    fillStage2(stage2);
    fillStage3(stage3);
    fillStage4(stage4);
    fillStage5(stage5);

    allButtons= findChildren<QPushButton*>();

    setAccessName();
    setSizePolicy();
    setOptions();

    current_language=Russian;
    setTextButtons(current_language);


    for(auto indx:allButtons){
        indx->setFont(QFont("Verdana",font_size));
        connect(indx,&QPushButton::clicked,this,&VirtualKeyboard::on_btn_click);
    }
    connect(btnShift,&QPushButton::toggled,this,&VirtualKeyboard::on_btnShift_toggle);
    connect(btnCapsLock,&QPushButton::toggled,this,&VirtualKeyboard::on_btnCaps_toggle);
    connect(btnLang,&QPushButton::clicked,this,&VirtualKeyboard::on_btnLang_click);

}


VirtualKeyboard::~VirtualKeyboard()
{

}

void VirtualKeyboard::on_btn_click()
{
    QPushButton *curBtn=dynamic_cast<QPushButton*>(sender());
    QString ch = curBtn->text().trimmed();

    ushort involvedKeys=1;
    bool isOk;
    int keyId=curBtn->accessibleName().toInt(&isOk,16);

    Qt::KeyboardModifiers Modifier = Qt::NoModifier;

    if (isShift) {
        Modifier = Modifier | Qt::ShiftModifier;
        involvedKeys++;
    }



    if(!checkIsServiceKey(keyId)){
        QKeyEvent keyEvent(QEvent::KeyPress, keyId, Modifier, ch, false, involvedKeys);
        QApplication::sendEvent(parent_widget->focusWidget(), &keyEvent);
        btnShift->setChecked(false);
    }






}

void VirtualKeyboard::on_btnLang_click()
{
    if(current_language==Russian){
        current_language=English;

    }else{
        current_language=Russian;
    }

    setTextButtons(current_language);

}

void VirtualKeyboard::on_btnShift_toggle(const bool state)
{
    isShift=state;

    if(state){
        setUpperText();
    }    else {
        setLowerText();
    }

}

void VirtualKeyboard::on_btnCaps_toggle(const bool state)
{

    isCaps=state;
    if(state){
        setUpperText();
    }    else {
        setLowerText();
    }

}

void VirtualKeyboard::initElements()
{

    mainLayout= new QVBoxLayout();

    stage1= new QHBoxLayout();
    stage2= new QHBoxLayout();
    stage3= new QHBoxLayout();
    stage4= new QHBoxLayout();
    stage5= new QHBoxLayout();


    btnTilt=new QPushButton();
    btn1=new QPushButton("1");
    btn2=new QPushButton("2");
    btn3=new QPushButton("3");
    btn4=new QPushButton("4");
    btn5=new QPushButton("5");
    btn6=new QPushButton("6");
    btn7=new QPushButton("7");
    btn8=new QPushButton("8");
    btn9=new QPushButton("9");
    btn0=new QPushButton("0");
    btnBackspace=new QPushButton(tr("Backspace"));
    btnTab=new QPushButton(tr("Tab"));
    btnQ=new QPushButton();
    btnW=new QPushButton();
    btnE=new QPushButton();
    btnR=new QPushButton();
    btnT=new QPushButton();
    btnY=new QPushButton();
    btnU=new QPushButton();
    btnI=new QPushButton();
    btnO=new QPushButton();
    btnP=new QPushButton();
    btnOpenSquare=new QPushButton();
    btnCloseSquare=new QPushButton();
    btnCapsLock=new QPushButton(tr("Caps Lock"));
    btnA=new QPushButton();
    btnS=new QPushButton();
    btnD=new QPushButton();
    btnF=new QPushButton();
    btnG=new QPushButton();
    btnH=new QPushButton();
    btnJ=new QPushButton();
    btnK=new QPushButton();
    btnL=new QPushButton();
    btnSemicolon=new QPushButton();
    btnSp=new QPushButton();
    btnShift=new QPushButton(tr("Shift"));
    btnZ=new QPushButton();
    btnX=new QPushButton();
    btnC=new QPushButton();
    btnV=new QPushButton();
    btnB=new QPushButton();
    btnN=new QPushButton();
    btnM=new QPushButton();
    btnLess=new QPushButton();
    btnMore=new QPushButton();
    btnSpace=new QPushButton(tr("Пробел"));
    btnLang=new QPushButton();



}

void VirtualKeyboard::setTextButtons(const VirtualKeyboard::Language p_lang)
{
    switch (p_lang) {
    case Russian:
        setRussianText();
        break;
    case English:
        setEnglishText();
        break;

    }
}

void VirtualKeyboard::setRussianText()
{
    btnTilt->setText(tr("ё"));

    btnQ->setText("й");
    btnW->setText("ц");
    btnE->setText("у");
    btnR->setText("к");
    btnT->setText("е");
    btnY->setText("н");
    btnU->setText("г");
    btnI->setText("ш");
    btnO->setText("щ");
    btnP->setText("з");
    btnOpenSquare->setText("х");
    btnCloseSquare->setText("ъ");



    btnA->setText("ф");
    btnS->setText("ы");
    btnD->setText("в");
    btnF->setText("а");
    btnG->setText("п");
    btnH->setText("р");
    btnJ->setText("о");
    btnK->setText("л");
    btnL->setText("д");
    btnSemicolon->setText("ж");
    btnSp->setText("э");

    btnZ->setText("я");
    btnX->setText("ч");
    btnC->setText("с");
    btnV->setText("м");
    btnB->setText("и");
    btnN->setText("т");
    btnM->setText("ь");
    btnLess->setText("б");
    btnMore->setText("ю");


    btnLang->setText(tr("Рус"));

}

void VirtualKeyboard::setEnglishText()
{

    btnTilt->setText("`");

    btnQ->setText("q");
    btnW->setText("w");
    btnE->setText("e");
    btnR->setText("r");
    btnT->setText("t");
    btnY->setText("y");
    btnU->setText("u");
    btnI->setText("i");
    btnO->setText("o");
    btnP->setText("p");
    btnOpenSquare->setText("[");
    btnCloseSquare->setText("]");



    btnA->setText("a");
    btnS->setText("s");
    btnD->setText("d");
    btnF->setText("f");
    btnG->setText("g");
    btnH->setText("h");
    btnJ->setText("j");
    btnK->setText("k");
    btnL->setText("l");
    btnSemicolon->setText(";");
    btnSp->setText("'");

    btnZ->setText("z");
    btnX->setText("x");
    btnC->setText("c");
    btnV->setText("v");
    btnB->setText("b");
    btnN->setText("n");
    btnM->setText("m");
    btnLess->setText(",");
    btnMore->setText(".");

    btnLang->setText(tr("Eng"));

}

void VirtualKeyboard::setLowerText()
{
    for(auto indx:allButtons){
        if(indx->accessibleName().isEmpty()){
            indx->setText(indx->text().toLower());
        }
    }
}

void VirtualKeyboard::setUpperText()
{
    for(auto indx:allButtons){
        if(indx->accessibleName().isEmpty()){
            indx->setText(indx->text().toUpper());
        }
    }
}

bool VirtualKeyboard::checkIsServiceKey(const int p_key)
{

    if (p_key==Qt::Key_Shift
            || p_key==Qt::Key_CapsLock
            || p_key==0x01ffffff){
        return true;
    }else{
        return false;
    }

}



void VirtualKeyboard::setAccessName()
{
    btnBackspace->setAccessibleName(tr("0x01000003"));
    btnTab->setAccessibleName(tr("0x01000001"));
    btnShift->setAccessibleName(tr("0x01000020"));
    btnSpace->setAccessibleName(tr("0x20"));
    btnCapsLock->setAccessibleName(tr("0x01000024"));
    btnLang->setAccessibleName(tr("0x01ffffff"));


}

void VirtualKeyboard::setSizePolicy()
{


    btnSpace->setSizePolicy(QSizePolicy::Expanding,QSizePolicy::Preferred);

}

void VirtualKeyboard::setOptions()
{
    btnShift->setCheckable(true);
    btnCapsLock->setCheckable(true);


}

void VirtualKeyboard::fillStage1(QHBoxLayout *p_lay)
{
    p_lay->addWidget(btnTilt);
    p_lay->addWidget(btn1);
    p_lay->addWidget(btn2);
    p_lay->addWidget(btn3);
    p_lay->addWidget(btn4);
    p_lay->addWidget(btn5);
    p_lay->addWidget(btn6);
    p_lay->addWidget(btn7);
    p_lay->addWidget(btn8);
    p_lay->addWidget(btn9);
    p_lay->addWidget(btn0);
    p_lay->addWidget(btnBackspace);

}

void VirtualKeyboard::fillStage2(QHBoxLayout *p_lay)
{
    p_lay->addWidget(btnTab);
    p_lay->addWidget(btnQ);
    p_lay->addWidget(btnW);
    p_lay->addWidget(btnE);
    p_lay->addWidget(btnR);
    p_lay->addWidget(btnT);
    p_lay->addWidget(btnY);
    p_lay->addWidget(btnU);
    p_lay->addWidget(btnI);
    p_lay->addWidget(btnO);
    p_lay->addWidget(btnP);
    p_lay->addWidget(btnOpenSquare);
    p_lay->addWidget(btnCloseSquare);
}

void VirtualKeyboard::fillStage3(QHBoxLayout *p_lay)
{
    p_lay->addWidget(btnCapsLock);
    p_lay->addWidget(btnA);
    p_lay->addWidget(btnS);
    p_lay->addWidget(btnD);
    p_lay->addWidget(btnF);
    p_lay->addWidget(btnG);
    p_lay->addWidget(btnH);
    p_lay->addWidget(btnJ);
    p_lay->addWidget(btnK);
    p_lay->addWidget(btnL);
    p_lay->addWidget(btnSemicolon);
    p_lay->addWidget(btnSp);
}

void VirtualKeyboard::fillStage4(QHBoxLayout *p_lay)
{
    p_lay->addWidget(btnShift);
    p_lay->addWidget(btnZ);
    p_lay->addWidget(btnX);
    p_lay->addWidget(btnC);
    p_lay->addWidget(btnV);
    p_lay->addWidget(btnB);
    p_lay->addWidget(btnN);
    p_lay->addWidget(btnM);
    p_lay->addWidget(btnLess);
    p_lay->addWidget(btnMore);


}

void VirtualKeyboard::fillStage5(QHBoxLayout *p_lay)
{

    p_lay->addWidget(btnSpace);
    p_lay->addWidget(btnLang);

}

void VirtualKeyboard::fillMainLay(QVBoxLayout *p_lay)
{
    p_lay->addLayout(stage1);
    p_lay->addLayout(stage2);
    p_lay->addLayout(stage3);
    p_lay->addLayout(stage4);
    p_lay->addLayout(stage5);

    setLayout(p_lay);

}
