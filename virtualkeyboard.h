#ifndef VIRTUALKEYBOARD_H
#define VIRTUALKEYBOARD_H


#include <QApplication>
#include <QKeyEvent>
#include <QWidget>
#include <QHBoxLayout>
#include <QVBoxLayout>
#include <QPushButton>


#if defined MAKE_VK_LIB
 #define VK_LIB_EXPORT Q_DECL_EXPORT
#else
 #define VK_LIB_EXPORT Q_DECL_IMPORT
#endif

class VirtualKeyboard final: public QWidget
{
    Q_OBJECT

public:

    enum Language{Russian,English};

    explicit VirtualKeyboard(const QWidget *p_parent, const int font_size);
    ~ VirtualKeyboard();

signals:

private slots:

    void on_btn_click();
    void on_btnShift_toggle(const bool state);
    void on_btnCaps_toggle(const bool state);
    void on_btnLang_click();



private:

    void initElements();



    void setTextButtons(const Language);
    void setRussianText();
    void setEnglishText();

    void setLowerText();
    void setUpperText();

    bool checkIsServiceKey(const int p_key);

    void setAccessName();
    void setSizePolicy();
    void setOptions();

    void fillMainLay(QVBoxLayout *p_lay);
    void fillStage1(QHBoxLayout *p_lay);
    void fillStage2(QHBoxLayout *p_lay);
    void fillStage3(QHBoxLayout *p_lay);
    void fillStage4(QHBoxLayout *p_lay);
    void fillStage5(QHBoxLayout *p_lay);



    Language current_language;


    bool isShift,isCaps;

    QList<QPushButton*>allButtons;

    QWidget *parent_widget;

    QVBoxLayout *mainLayout;
    QHBoxLayout *stage1;
    QHBoxLayout *stage2;
    QHBoxLayout *stage3;
    QHBoxLayout *stage4;
    QHBoxLayout *stage5;



    QPushButton *btnTilt;
    QPushButton *btn1;
    QPushButton *btn2;
    QPushButton *btn3;
    QPushButton *btn4;
    QPushButton *btn5;
    QPushButton *btn6;
    QPushButton *btn7;
    QPushButton *btn8;
    QPushButton *btn9;
    QPushButton *btn0;
    QPushButton *btnBackspace;
    QPushButton *btnTab;
    QPushButton *btnQ;
    QPushButton *btnW;
    QPushButton *btnE;
    QPushButton *btnR;
    QPushButton *btnT;
    QPushButton *btnY;
    QPushButton *btnU;
    QPushButton *btnI;
    QPushButton *btnO;
    QPushButton *btnP;
    QPushButton *btnOpenSquare;
    QPushButton *btnCloseSquare;
    QPushButton *btnCapsLock;
    QPushButton *btnA;
    QPushButton *btnS;
    QPushButton *btnD;
    QPushButton *btnF;
    QPushButton *btnG;
    QPushButton *btnH;
    QPushButton *btnJ;
    QPushButton *btnK;
    QPushButton *btnL;
    QPushButton *btnSemicolon;
    QPushButton *btnSp;
    QPushButton *btnShift;
    QPushButton *btnZ;
    QPushButton *btnX;
    QPushButton *btnC;
    QPushButton *btnV;
    QPushButton *btnB;
    QPushButton *btnN;
    QPushButton *btnM;
    QPushButton *btnLess;
    QPushButton *btnMore;
    QPushButton *btnSpace;
    QPushButton *btnLang;



};

#endif // VIRTUALKEYBOARD_H
